from django.contrib.auth import (
    REDIRECT_FIELD_NAME,
    login as auth_login,
)
from django.contrib.auth.forms import (
    AuthenticationForm
)
from django.contrib.auth.views import _get_login_redirect_url
from django.contrib.sites.shortcuts import get_current_site
from django.http import JsonResponse
from django.template.response import TemplateResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

from common.utils import jsonify_form_errors

from .models import User
from .serializers import UserSerializer


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='accounts/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          extra_context=None, redirect_authenticated_user=False):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name, request.GET.get(
        redirect_field_name, ''))

    if redirect_authenticated_user and request.user.is_authenticated:
        redirect_to = _get_login_redirect_url(request, redirect_to)
        return JsonResponse({'redirect_to': redirect_to})
    elif request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
            redirect_to = _get_login_redirect_url(request, redirect_to)
            return JsonResponse({'redirect_to': redirect_to})
        else:
            response = JsonResponse({
                'errors': jsonify_form_errors(form)
            })
            response.status_code = 400
            return response
    else:
        form = authentication_form(request)
    current_site = get_current_site(request)
    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context)
