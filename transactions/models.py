from django.db import models
from django.utils import timezone

from django_fsm import FSMField
from djmoney.models.fields import MoneyField


class Transaction(models.Model):
    # STATUS_CHOICES = (
    #     (1, 'new'),
    #     (2, 'accepted'),
    #     (3, 'finished'),
    # )
    L_PERSONA_CHOICES = (
    )

    lender = models.ForeignKey(
        'accounts.User', related_name='lender_transactions')
    borrower = models.ForeignKey(
        'accounts.User', related_name='borrower_transactions')
    # d_lender = models.CharField()
    b_date = models.DateTimeField(default=timezone.now)
    p_date = models.DateTimeField(null=True, blank=True)
    b_amount = MoneyField(
        max_digits=10, decimal_places=2, default_currency='PEN')
    p_amount = MoneyField(
        max_digits=10, decimal_places=2, default_currency='PEN')
    status = FSMField(default='new')
    tax = models.DecimalField(max_digits=4, decimal_places=2)
    fees = models.PositiveIntegerField()


class Payment(models.Model):
    transaction = models.ForeignKey(Transaction)
    fee_number = models.PositiveIntegerField(default=1)
    p_date = models.DateTimeField(default=timezone.now)
    e_date = models.DateTimeField(default=timezone.now)
    p_amount = MoneyField(
        max_digits=10, decimal_places=2, default_currency='PEN')
    tax = models.DecimalField(max_digits=4, decimal_places=2)
    d_tax = models.DecimalField(max_digits=4, decimal_places=2)

    class Meta:
        unique_together = ('transaction', 'fee_number')
