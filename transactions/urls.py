from django.conf.urls import url

from transactions import views


urlpatterns = [
    url(r'^$', views.MainView.as_view(), name=''),
]
