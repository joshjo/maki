from django import forms

from .models import Transaction


class TransactionApiForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ('b_amount', 'tax', 'fees')
