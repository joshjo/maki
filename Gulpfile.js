var gulp = require('gulp'),
    bootstrap = require('bootstrap-styl'),
    stylus = require('gulp-stylus'),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),
    path = require('path');


var paths = {
    styles: './static/styl/**/*.styl',
    icons: './static/icons/*.svg',
    templates: './templates'
}

gulp.task('styles', function() {
    return gulp.src(['./static/styl/maki.styl', './static/styl/login.styl',])
        .pipe(stylus({ use: bootstrap(), compress: true }))
        .pipe(gulp.dest('./static/styl/'))
});

gulp.task('icons', function () {
    return gulp
        .src(paths.icons)
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore({
            inlineSvg: true
        }))
        .pipe(gulp.dest(paths.templates));
});


gulp.task('watch', function () {
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.icons, ['icons']);
});

gulp.task('default', function() {
    console.log('Hello world.');
});
