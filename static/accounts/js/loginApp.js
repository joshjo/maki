var registrationApp = new Vue({
    el: '#login-app',
    data: {
        active: 'login',
        models: {
            login: {
                username: '',
                password: '',
            },
        },
        classes: {
            signup: 'signup',
            login: 'login',
        }
    },
    methods: {
        login: function(e) {
            e.preventDefault();
            console.log(e);
        },
    }
});
