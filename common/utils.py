def jsonify_form_errors(form):
    return {
        f: e.get_json_data(False) for f, e in form.errors.items()
    }
