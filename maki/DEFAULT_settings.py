from .settings_base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'maki',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

# ----------------- BEGIN DEVELOPMENT PARAMETERS -----------------
# Please set this parameters only for development
TASTYPIE_FULL_DEBUG = True

# ----------------- END DEVELOPMENT PARAMETERS -----------------
